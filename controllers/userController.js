let mysql = require("mysql");
let bcrypt = require("bcrypt-nodejs");
let moment = require("moment");

class UserController {

    constructor(req, res, connection) {
        this.connection = connection;
        this.req = req;
        this.res = res;
    }

    getUsers() {
        let query = "SELECT * FROM ??";
        let table = ["users"];
        query = mysql.format(query,table);
        this.connection.query(query, (err,rows) => {
            if(err) {
                this.res.status(400).json({"err" : "db_err", "full":err});
            } else {
                this.res.json(rows);
            }
        });
    }

    getUser() {
        let query = "SELECT * FROM ?? WHERE ??=?";
        let table = ["users","id",this.req.params.user_id];
        query = mysql.format(query,table);
        this.connection.query(query, (err,rows) => {
            if(err) {
                this.res.status(400).json({"err" : "db_err", "full":err});
            } else {
                if(rows[0]) {
                    this.res.json(rows[0]);
                } else {
                    this.res.status(404).json({"err" : "not_found"});
                }
            }
        });
    }

    postUser() {
        let query = "INSERT INTO ??(??,??) VALUES (?,?)";
        let table = ["users","email","password",this.req.body.email,bcrypt.hashSync(this.req.body.password) ];
        query = mysql.format(query,table);
        this.connection.query(query, (err,rows) => {
            if(err) {
                this.res.status(400).json({"err" : "db_err", "full":err});
            } else {
                this.res.json(true);
            }
        });
    }

    putUser() {
        let timestamp = moment(Date.now()).format('YYYY-MM-DD HH:mm:ss');
        let query = "UPDATE ?? SET ?? = ?, ?? = ?, ?? = ? WHERE ?? = ?";
        let table = ["users",
            "password",bcrypt.hashSync(this.req.body.password),
            "email",this.req.body.email,
            "updated_at",timestamp,
            "id",this.req.body.id];
        query = mysql.format(query,table);
        this.connection.query(query, (err,rows) => {
            if(err) {
                this.res.status(400).json({"err" : "db_err", "full": err});
            } else {
                this.res.json(true);
            }
        });
    }

    deleteUser() {
        let query = "DELETE from ?? WHERE ??=?";
        let table = ["users","id",this.req.params.user_id];
        query = mysql.format(query,table);
        this.connection.query(query,(err,rows) => {
            if(err) {
                this.res.status(400).json({"err" : "db_err", "full": err});
            } else {
                this.res.json(true);
            }
        });
    }
}

module.exports = UserController;
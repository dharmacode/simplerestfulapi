# Stawianie projektu
Nasz projekt bazuje na tutorialu [RESTful API Using Node and Express 4](https://codeforgeek.com/2015/03/restful-api-node-and-express-4/)
ma podpięty prosty framework [express](http://expressjs.com/) oraz obsługuje częściowo [ES6](http://es6-features.org).  

1. Aby uruchomić projekt musisz miec zainstalowane w systemie nodejs oraz npm. (tzn. z poziomu konsoli komenda *npm* powinna być dostępna). Wchodzimy do katalogu projektu i wpisujemy:
```
npm install
```
2. Następnie w dodajemy nową bazę danych mysql z kodowaniem znaków **utf8_unicode_ci** (to uchroni nas przed błędami sortowania w nietypowych językach).
1. Kopiujemy plik *.env.exaple* jako *.env* a następnie edytujemy go wprowadzając dane dostępowe do utworzonej wcześniej bazy danych oraz port na jakim ma uruchomić sie serwer
1. Następnie uruchamiamy skrypty migracji:
```
node node_modules/db-migrate/bin/db-migrate up
```
5. Jeżeli jest potrzeba cofnięcia migracji (np. przy resecie bazy) to używamy do tego polecenia:
```
node node_modules/db-migrate/bin/db-migrate down
```

# Rozwijanie projektu


#### Dodawanie nowego pliku migracji 
Robimy to za pomocą polecenia:
```
node node_modules/db-migrate/bin/db-migrate create nazwa_nowej_tabeli --sql-file
```
To spowoduje utworzenie 1 pliku .js w katalogu `./migrations` i 2 plików .sql w katalogu `./migrations/sqls` my będziemy edytować pliki sql.
Plik zakończony na `...-up.sql` zawiera polecenia tworzace/zmieniające tabelę. Plik zakończony na `...-down.sql` zawiera polecenia mające cofać zmiany.

#### Dodawanie routingu i jego obsługi

W pliku `REST.js` znajduje się przykładowy routing restfulowy dla obiektu *User*, obsługa requestów znajduje się w katalogu './controllers/userController.js'.
Wprowadzając nowy obiekt należy skopiować routingi User-a oraz jego kontroler odpowiednio zmieniając.
let UserController = require('./controllers/userController.js');

function REST_ROUTER(router,connection,md5) {
    let self = this;
    self.handleRoutes(router,connection,md5);
}

REST_ROUTER.prototype.handleRoutes= function(router,connection,md5) {
    router.get("/",function(req,res){
        res.json({"Message" : "Hello World !"});
    });

    router.get("/routing-details", (req,res) => {
        if(process.env.DEBUG=='true') {
            res.json({routes: router.stack });
        } else {
            res.json({routes: null});
        }
    });

    router.post("/users",            (req,res) => { (new UserController(req,res,connection)).postUser(); });
    router.get("/users",             (req,res) => { (new UserController(req,res,connection)).getUsers(); });
    router.get("/users/:user_id",    (req,res) => { (new UserController(req,res,connection)).getUser(); });
    router.put("/users",             (req,res) => { (new UserController(req,res,connection)).putUser(); });
    router.delete("/users/:user_id", (req,res) => { (new UserController(req,res,connection)).deleteUser(); });

};

module.exports = REST_ROUTER;
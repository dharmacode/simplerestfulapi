let express = require("express");
let mysql   = require("mysql");
let bodyParser  = require("body-parser");
let md5 = require('MD5');
let rest = require("./REST.js");
let app  = express();
require('dotenv').config();

//let env = require('node-env-file');
//node-env-file
function REST(){
    let self = this;
    //env(__dirname + '/.env');
    self.connectMysql();
};

REST.prototype.connectMysql = function() {
    let self = this;
    let pool      =    mysql.createPool({
        connectionLimit : 100,
        host     : process.env.DB_HOST,
        user     : process.env.DB_USER,
        password : process.env.DB_PASS,
        database : process.env.DB_NAME,
        debug    :  false
    });
    pool.getConnection(function(err,connection){
        if(err) {
            self.stop(err);
        } else {
            self.configureExpress(connection);
        }
    });
};

REST.prototype.configureExpress = function(connection) {
    let self = this;
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());
    let router = express.Router();
    app.use('/api', router);
    let rest_router = new rest(router,connection,md5);
    self.startServer();
};

REST.prototype.startServer = function() {
    app.listen(process.env.PORT,function(){
        console.log("All right ! I am alive at Port " + process.env.PORT );
    });
};

REST.prototype.stop = function(err) {
    console.log("ISSUE WITH MYSQL n" + err);
    process.exit(1);
};

new REST();